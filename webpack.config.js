const Encore = require('@symfony/webpack-encore');
const webpack = require('webpack');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .enableStimulusBridge('./assets/controllers.json')
    .autoProvidejQuery()

    .addStyleEntry('loader_css', './assets/styles/loader.css')
    .addStyleEntry('izi_toast_css', './node_modules/izitoast/dist/css/iziToast.min.css')

    .addEntry('app', [
        './assets/js/app.js',
        './node_modules/@popperjs/core/dist/umd/popper.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.bundle.js',
    ])
    .addEntry('stimulus_controller', './assets/bootstrap.js')
    .addEntry('bootstrap_file_js', [
        './node_modules/bootstrap-fileinput/js/fileinput.js',
        './node_modules/bootstrap-fileinput/js/fileinput_locale_LANG.js',
        './node_modules/bootstrap-fileinput/js/plugins/canvas-to-blob.js',
        './assets/js/fileinput_custom_settings.js',
    ])
    .addEntry('lightBox2_js', [
        './node_modules/lightbox2/dist/js/lightbox.min.js',
        './node_modules/lightbox2/dist/js/lightbox-plus-jquery.min.js',
    ])
    .addEntry('products_filter', './assets/js/products-filter.js')
    .addEntry('gallery-resizer', './assets/js/gallery-resizer.js')

    .addStyleEntry('bootstrap_file_css', './node_modules/bootstrap-fileinput/css/fileinput.css')
    .addStyleEntry('lightBox2_css', './node_modules/lightbox2/dist/css/lightbox.min.css')
    .addStyleEntry('breadcrumb_css', './assets/styles/breadcrumb.css')
    .addStyleEntry('pagination', './assets/styles/pagination.css')

    .splitEntryChunks()
    .enableSassLoader()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .cleanupOutputBeforeBuild()
    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    .addPlugin(new webpack.ProvidePlugin({
        bootstrap: 'bootstrap'
    }))

    .copyFiles([
        {
            from: './public/img',
            to: 'images/[path][name].[hash:8].[ext]',
        },
        {
            from: './public/uploads',
            to: 'images/[name].[ext]'
        }
    ])
;

module.exports = Encore.getWebpackConfig();
