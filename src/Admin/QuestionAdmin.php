<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Question;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

final class QuestionAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $filter
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $filter): void
    {
        $filter
            ->add('id')
            ->add('sender')
            ->add('phone')
            ->add('content');
    }

    /**
     * @param ListMapper $list
     *
     * @return void
     */
    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('id')
            ->add('sender')
            ->add('phone')
            ->add('content')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'delete' => [],
                ],
            ]);
    }

    /**
     * @param ShowMapper $show
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('id')
            ->add('sender')
            ->add('phone')
            ->add('content');
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection
            ->remove('create')
            ->remove('edit');
    }

    /**
     * @param object $object
     *
     * @return string
     */
    public function toString($object): string
    {
        return $object instanceof Question
            ? $object->getSender()
            : 'Question';
    }
}
