<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Realization;
use App\Util\FileUploader;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class RealizationAdmin extends AbstractAdmin
{
    private $fileUploader;

    /**
     * @param $code
     * @param $class
     * @param $baseControllerName
     * @param FileUploader $fileUploader
     */
    public function __construct($code, $class, $baseControllerName, FileUploader $fileUploader)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->fileUploader = $fileUploader;
    }

    /**
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('mainImage', null, [
                'template' => 'admin/realization/mainImage.html.twig',
            ])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('properties', ModelType::class, [
                'error_bubbling' => true,
                'property' => 'icon',
                'multiple' => true,
                'expanded' => true,
                'btn_add' => false,
            ])
            ->add('products', ModelType::class , [
                'error_bubbling' => true,
                'property' => 'name',
                'multiple' => true,
                'expanded' => false,
                'btn_add' => false,
            ])
            ->add('mainImageFile', FileType::class, [
                'error_bubbling' => true,
                'required' => false,
                'attr' => [
                    'class' => 'file',
                ],
            ])
        ;
    }

    /**
     * @param object $object
     *
     * @return string
     */
    public function toString($object): string
    {
        return $object instanceof Realization
            ? sprintf('Realization %s', $object->getId())
            : 'Realization';
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection
            ->remove('show')
        ;
    }

    /**
     * @param Realization $object
     *
     * @return void
     *
     * @throws \Exception
     */
    public function prePersist($object): void
    {
        if($object->getMainImageFile() instanceof UploadedFile) {
            $name = $this->fileUploader->upload($object->getMainImageFile());
            $object->setMainImage($name);
        }
    }

    /**
     * @param Realization $object
     *
     * @return void
     *
     * @throws \Exception
     */
    public function preUpdate($object): void
    {
        if($object->getMainImageFile() instanceof UploadedFile) {
            $name = $this->fileUploader->upload($object->getMainImageFile());
            $object->setMainImage($name);
        }
    }
}
