<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Property;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\FileType;

final class PropertyAdmin extends AbstractAdmin
{
    /**
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('id')
            ->add('name')
            ->add('icon', null, [
                'template' => 'admin/property/iconField.html.twig'
            ])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('name', null, [
                'error_bubbling' => true,
            ])
            ->add('description', null, [
                'error_bubbling' => true,
            ])
            ->add('file', FileType::class, [
                'error_bubbling' => true,
                'required' => false,
                'label' => 'Icon',
                'attr' => [
                    'class' => 'file',
                    'data-preview-file-type' => 'text'
                ],
            ])
        ;
    }

    /**
     * @param object $object
     *
     * @return string
     */
    public function toString($object): string
    {
        return $object instanceof Property
            ? $object->getName()
            : 'Property';
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection
            ->remove('show')
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function prePersist($object): void
    {
        $rootDir = $this->getConfigurationPool()->getContainer()->getParameter('kernel.root_dir');
        $path = $rootDir. '/../'. Property::UPLOADS;

        $object->uploadImage($path);
    }

    /**
     * {@inheritDoc}
     */
    public function preUpdate($object): void
    {
        $rootDir = $this->getConfigurationPool()->getContainer()->getParameter('kernel.root_dir');
        $path = $rootDir. '/../'. Property::UPLOADS;

        $object->uploadImage($path);
    }
}
