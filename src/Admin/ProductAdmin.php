<?php

declare(strict_types=1);

namespace App\Admin;

use Behat\Transliterator\Transliterator;
use Sonata\Form\Type\CollectionType;
use App\Entity\Product;
use App\Entity\Property;
use App\Util\FileUploader;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class ProductAdmin extends AbstractAdmin
{
    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * @param $code
     * @param $class
     * @param $baseControllerName
     * @param FileUploader $fileUploader
     */
    public function __construct($code, $class, $baseControllerName, FileUploader $fileUploader)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->fileUploader = $fileUploader;
    }

    /**
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('name')
            ->add('description')
            ->add('price', null, [
                'editable' => true,
            ])
            ->add('priority', null, [
                'editable' => true,
            ])
            ->add('category.name')
            ->add('mainImage', null, [
                'template' => 'admin/product/imageField.html.twig'
            ])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                    'clone' => [
                        'template' => 'admin/product/list_clone_btn.html.twig',
                    ]
                ],
            ]);
    }

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('name', null, [
                'error_bubbling' => true,
            ])
            ->add('priority', null, [
                'error_bubbling' => true,
            ])
            ->add('description', null, [
                'error_bubbling' => true,
            ])
            ->add('longDescription', null, [
                'error_bubbling' => true,
            ])
            ->add('price', null, [
                'error_bubbling' => true,
            ])
            ->add('currency', ChoiceType::class, [
                'choices' => [
                    'PLN' => 'PLN',
                    'EUR' => 'EUR',
                    'USD' => 'USD',
                ]
            ])
            ->add('properties', ModelType::class, [
                'error_bubbling' => true,
                'class' => Property::class,
                'property' => 'icon',
                'multiple' => true,
                'expanded' => true,
                'btn_add' => false,
            ])
            ->add('mainFileImage', FileType::class, [
                'error_bubbling' => true,
                'label' => 'Main Image',
                'required' => false,
                'attr' => [
                    'class' => 'file product-main-img',
                ],
            ])
            ->add('secondFileImage', FileType::class, [
                'error_bubbling' => true,
                'label' => 'Second file Image',
                'required' => false,
                'attr' => [
                    'class' => 'file product-obj-img',
                ],
            ])
            ->add('images', CollectionType::class, [
                'error_bubbling' => true,
                'type_options' => [
                    'delete' => true,
                ],
            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ])
        ;
    }

    /**
     * @param object $object
     *
     * @return string
     */
    public function toString($object): string
    {
        return $object instanceof Product && $object->getName()
            ? $object->getName()
            : 'Product';
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection
            ->remove('show')
            ->add('clone', $this->getRouterIdParameter().'/clone')
        ;
    }

    /**
     * @param Product $object
     *
     * @throws \Exception
     */
    public function prePersist($object): void
    {
        $this->handleUploadedImages($object);

        $object->setSlug(Transliterator::transliterate($object->getName()));
    }

    /**
     * @param Product $object
     *
     * @throws \Exception
     */
    public function preUpdate($object): void
    {
        $this->handleUploadedImages($object);
    }

    /**
     * @param Product $product
     *
     * @return void
     *
     * @throws \Exception
     */
    private function handleUploadedImages(Product $product): void
    {
        if($product->getMainFileImage() instanceof UploadedFile) {
            $name = $this->fileUploader->upload($product->getMainFileImage());
            $product->setMainImage($name);
        }

        if($product->getSecondFileImage() instanceof UploadedFile) {
            $name = $this->fileUploader->upload($product->getSecondFileImage());
            $product->setSecondImage($name);
        }

        foreach ($product->getImages() as $image){
            if($image->getFile() instanceof UploadedFile){
                $name = $this->fileUploader->upload($image->getFile());
                $image->setImage($name);
                $image->setProduct($product);
            }
        }
    }
}
