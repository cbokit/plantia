<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

final class ProductImageAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('position', NumberType::class, [
                'error_bubbling' => true,
            ])
            ->add('image', HiddenType::class, [
                'required' => false,
            ])
            ->add('file', FileType::class, [
                'error_bubbling' => true,
                'required' => false,
                'attr' => [
                    'accept' => "image/*",
                    'class' => 'fileUpload',
                ]
            ]);
    }
}
