<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Article;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\FileType;

final class ArticleAdmin extends AbstractAdmin
{
    /**
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('id')
            ->add('title')
            ->add('content')
            ->add('largeImage', null, [
                'template' => 'admin/article/largeImageField.html.twig',
            ])
            ->add('smallImage', null, [
                'template' => 'admin/article/smallImageField.html.twig',
            ])
            ->add('ogSmallImage', null, [
                'template' => 'admin/article/ogSmallImageField.html.twig',
            ])
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('title', null, [
                'error_bubbling' => true,
            ])
            ->add('content', CKEditorType::class, [
                'error_bubbling' => true,
                'config' => [
                    'filebrowserImageBrowseRoute' => 'ckeditor_image_browse',
                    'filebrowserImageUploadRoute' => 'ckeditor_image_upload',
                ],
            ])
            ->add('smallImgFile', FileType::class, [
                'label' => 'Small image',
                'error_bubbling' => true,
                'required' => false,
                'attr' => [
                    'class' => 'file article-file',
                    'data-preview-file-type' => 'text'
                ],
            ])
            ->add('ogSmallImgFile', FileType::class, [
                'label' => 'Open graph image',
                'error_bubbling' => true,
                'required' => false,
                'attr' => [
                    'class' => 'file article-file',
                    'data-preview-file-type' => 'text'
                ],
            ])
            ->add('largeImgFile', FileType::class, [
                'label' => 'Large Image',
                'error_bubbling' => true,
                'required' => false,
                'attr' => [
                    'class' => 'file article-file',
                    'data-preview-file-type' => 'text'
                ],
            ])
        ;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection
            ->remove('show');
    }

    /**
     * @param object $object
     *
     * @return string
     */
    public function toString($object): string
    {
        return $object instanceof Article
            ? $object->getTitle()
            : 'Article';
    }

    /**
     * {@inheritDoc}
     */
    public function prePersist($object): void
    {
        $rootDir = $this->getConfigurationPool()->getContainer()->getParameter('kernel.root_dir');
        $path = $rootDir. '/../'. Article::UPLOADS;

        $object->uploadImages($path);
    }

    /**
     * {@inheritDoc}
     */
    public function preUpdate($object): void
    {
        $rootDir = $this->getConfigurationPool()->getContainer()->getParameter('kernel.root_dir');
        $path = $rootDir. '/../'. Article::UPLOADS;

        $object->uploadImages($path);
    }
}
