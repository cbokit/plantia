<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\GalleryImage;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\FileType;

final class GalleryImageAdmin extends AbstractAdmin
{
    /**
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('position', null, [
                'editable' => true,
            ])
            ->add('image', null, [
                'template' => 'admin/gallery/image.html.twig'
            ])
            ->add('description')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('position', null, [
                'error_bubbling' => true,
            ])
            ->add('description', null, [
                'error_bubbling' => true,
            ])
            ->add('file', FileType::class, [
                'error_bubbling' => true,
                'label' => 'Image',
                'required' => false,
                'attr' => [
                    'class' => 'file',
                ],
            ])
        ;
    }

    /**
     * @param object $object
     *
     * @return string
     */
    public function toString($object): string
    {
        return $object instanceof GalleryImage
            ? $object->getImage()
            : 'Gallery\'s image';
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection
            ->remove('show');
    }

    /**
     * {@inheritDoc}
     */
    public function prePersist($object): void
    {
        $rootDir = $this->getConfigurationPool()->getContainer()->getParameter('kernel.root_dir');
        $path = $rootDir . '/../' . GalleryImage::UPLOADS;

        $object->uploadImage($path);
    }

    /**
     * {@inheritDoc}
     */
    public function preUpdate($object): void
    {
        $rootDir = $this->getConfigurationPool()->getContainer()->getParameter('kernel.root_dir');
        $path = $rootDir . '/../' . GalleryImage::UPLOADS;

        $object->uploadImage($path);
    }
}
