<?php

namespace App\Util;

final class TextUtil
{
    /**
     * @param string $originString
     * @param int $position
     * @param int $positionStart
     *
     * @return string
     */
    public static function sliceWord(string $originString, int $position, int $positionStart = 0): string
    {
        if (strlen($originString) > $position){
            $string  = wordwrap($originString, $position);
            $string  = substr($string, $positionStart, strpos($string, "\n"));
            $string .= '...';

            return $string;
        }

        return $originString;
    }
}