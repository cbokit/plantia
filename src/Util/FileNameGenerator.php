<?php

namespace App\Util;

final class FileNameGenerator
{
    /**
     * @param int $length
     *
     * @return string
     *
     * @throws \Exception
     */
    public static function generate(int $length = 50): string
    {
        $str = '';

        while(strlen($str) < $length){
            $str .= uniqid('', true);
            $str .= random_int(1000, 100000);
            $str = str_replace('.', '', $str);
        }

        return substr($str, 0, $length);
    }
}