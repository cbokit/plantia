<?php

namespace App\Util;

use Symfony\Component\HttpFoundation\File\UploadedFile;

final class FileUploader
{
    /**
     * @var string
     */
    private $uploadsDir;

    /**
     * @param string $uploadsDir
     */
    public function __construct(string $uploadsDir)
    {
        $this->uploadsDir = $uploadsDir;
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     *
     * @throws \Exception
     */
    public function upload(UploadedFile $file): string
    {
        $newName = FileNameGenerator::generate(70) . '.' . $file->getClientOriginalExtension();
        $file->move($this->uploadsDir, $newName);

        return $newName;
    }
}