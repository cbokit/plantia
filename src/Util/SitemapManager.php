<?php

namespace App\Util;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class SitemapManager
{
    private const WEEKLY = 'weekly';

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param UrlGeneratorInterface $router
     * @param EntityManagerInterface $em
     */
    public function __construct(UrlGeneratorInterface $router, EntityManagerInterface $em)
    {
        $this->router = $router;
        $this->em     = $em;
    }

    /**
     * @return string[]
     */
    public function getAll(): array
    {
        return array_merge($this->getDefaultsUrls(), $this->generateArticleUrls());
    }

    /**
     * @return string[]
     */
    public function getDefaultsUrls(): array
    {
        return [
            [
                'loc' => $this->router->generate('article_list', [], UrlGeneratorInterface::ABSOLUTE_URL),
                'changefreq' => self::WEEKLY,
                'priority'   => 1,
            ],
            [
                'loc' => $this->router->generate('home_article_page', [], UrlGeneratorInterface::ABSOLUTE_URL),
                'changefreq' => self::WEEKLY,
                'priority'   => 1,
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function generateArticleUrls(): array
    {
        $articles = $this->em->getRepository(Article::class)->findAll();
        $urls     = [];

        foreach ($articles as $article){
            $urls[] = [
                'loc' => $this->router->generate(
                    'article_show',
                    ['id' => $article->getId(), 'slug' => $article->getSlug()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                'changefreq' => self::WEEKLY,
                'priority'   => 0.8,
            ];
        }

        return $urls;
    }
}