<?php

namespace App\Form;

use App\Entity\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class QuestionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('sender', EmailType::class, [
                'attr' => [
                    'class' => 'validate form-control',
                    'placeholder' => 'Twój mail',
                    'data-validate' => 'required,email'
                ],
            ])
            ->add('phone', TextType::class, [
                'attr' => [
                    'class' => 'validate form-control',
                    'placeholder' => 'Numer telefonu',
                    'data-validate' => 'required,integer,min'
                ]
            ])
            ->add('content', TextareaType::class, [
                'attr' => [
                    'class' => 'validate form-control',
                    'placeholder' => 'Treść wiadomości...',
                    'data-validate' => 'required',
                    'cols' => 30,
                    'rows' => 8,
                ],
            ])
            ->add('isAgree', CheckboxType::class, [
                'attr' => [
                    'class' => 'validate form-control',
                    'data-validate' => 'required',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Wyślij',
                'attr' => [
                    'class' => 'btn btn__grey_transparent',
                    'formnovalidate' => true,
                    'data-action' => 'question#send',
                ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
            'attr' => [
                'autocomplete' => 'off',
            ],
        ]);
    }
}
