<?php

namespace App\Form;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

final class ProductsFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('filter', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                'label' => 'Filtruj',
                'placeholder' => 'Wszystkie produkty',
                'query_builder' => function(CategoryRepository $repository){
                    return $repository->createQueryBuilder('c')
                        ->innerJoin('c.products', 'p');
                },
                'choice_attr' => function($choice, $key) {
                    return [
                        'data-group' => $choice->getName(),
                        'value' => $key,
                    ];
                },
                'attr' => [
                    'class' => 'form-select filter-options',
                    'id' => 'filter',
                    'aria-label' => 'Wszystkie produkty'
                ],
                'label_attr' => [
                    'class' => 'h5 mb-5 file_download__filter_header',
                ],
            ])
        ;
    }
}
