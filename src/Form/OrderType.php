<?php

namespace App\Form;

use App\Entity\Order;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class OrderType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('width', TextType::class, [
                'attr' => [
                    'placeholder' => 'Szerokość w cm',
                    'class' => 'validate form-control input-black placeholder-gray',
                    'data-validate' => 'integer',
                ],
            ])
            ->add('height', TextType::class, [
                'attr' => [
                    'class' => 'validate form-control input-black placeholder-gray',
                    'placeholder' => 'Wysokość w cm',
                    'data-validate' => 'integer',
                ],
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => 'validate form-control input-black placeholder-gray',
                    'placeholder' => 'Email',
                    'data-validate' => 'requiredPl,emailPl'
                ],
            ])
            ->add('product', EntityType::class, [
                'class' => Product::class,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'form-select',
                    'data-order-target' => 'products',
                    'data-action' => 'change->order#selectProduct',
                ],
                'choice_attr' => function (Product $choice){
                    return [
                        'data-img' => sprintf('/uploads/%s', $choice->getMainImage()),
                    ];
                },
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'wyślij',
                'attr' => [
                    'class' => 'btn btn__black w-100',
                    'formnovalidate' => true,
                    'data-action' => 'order#send',
                ],
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
            'attr' => [
                'class' => 'row order-form',
                'autocomplete' => 'off',
            ],
        ]);
    }
}
