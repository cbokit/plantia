<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ContactType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('phone', TextType::class, [
                'attr' => [
                    'class' => 'validate form-control input-black',
                    'placeholder' => 'Twój numer',
                    'data-validate' => 'requiredPl,integer,min'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'wyślij',
                'attr' => [
                    'class' => 'btn btn__black w-100',
                    'formnovalidate' => true,
                    'data-action' => 'contact#sendContact'
                ],
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
            'attr' => [
                'autocomplete' => 'off',
                'data-controller' => 'contact',
                'data-contact-target' => 'form'
            ],
        ]);
    }
}
