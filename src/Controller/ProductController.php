<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Repository\RealizationRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/products")
 */
final class ProductController extends AbstractController
{
    /**
     * @Route("", name="products_index")
     *
     * @param ProductRepository $productRepository
     * @param RealizationRepository $realizationRepository
     *
     * @return Response
     */
    public function index(ProductRepository $productRepository, RealizationRepository $realizationRepository): Response
    {
        return $this->render('product/index.html.twig',[
            'products' => $productRepository->findBy([], ['priority' => 'ASC']),
            'lpProduct' => $productRepository->findOneBy([], ['price' => 'ASC']),
            'realizations' => $realizationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{slug}-{id}", name="show_product", requirements={"slug"=".+"})
     * @ParamConverter("product", class="App\Entity\Product")
     *
     * @param Product $product
     * @param string $slug
     * @param ProductRepository $productRepository
     *
     * @return Response
     */
    public function show(string $slug, Product $product, ProductRepository $productRepository): Response
    {
        if($slug !== $product->getSlug()){
            return $this->redirectToRoute('article_show', [
                'id' => $product->getId(),
                'slug' => $product->getSlug()
            ]);
        }

        return $this->render('product/show.html.twig', [
            'product'  => $product,
            'products' => $productRepository->getAllExpect($product->getId()),
        ]);
    }
}
