<?php

namespace App\Controller;

use App\Model\InternalResponse;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/process")
 */
final class ProcessController extends AbstractController
{
    /**
     * @Route("/build-webpack-cache", name="admin_build_webpack_cache", methods={"POST"})
     *
     * @param LoggerInterface $logger
     *
     * @return JsonResponse
     */
    public function buildWebpackCache(LoggerInterface $logger): JsonResponse
    {
        $projectDir = $this->getParameter('kernel.project_dir');
        $responseData = new InternalResponse('The cache was successfully rebuilt.');
        $msg = 'Process log' . PHP_EOL;

        try {
            $process = new Process(['npm', 'run', 'build']);
            $process->setWorkingDirectory($projectDir);
            $process->start();

            foreach ($process as $data) {
                $msg .= $data . PHP_EOL;
            }

            $logger->notice($msg);
        } catch (\Throwable $ex){
            $logger->warning($ex->getMessage());
            $responseData->error($ex->getMessage());
        }

        return new JsonResponse($responseData->toArray());
    }
}