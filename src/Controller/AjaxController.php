<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Exception\FormInvalidArgumentException;
use App\Form\ContactType;
use App\Model\InternalResponse;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

final class AjaxController extends AbstractController
{
    /**
     * @Route("/send-contact", name="send_contact", methods={"POST"})
     */
    public function sendProductQuestion(Request $request, MailerInterface $mailer, LoggerInterface $logger, EntityManagerInterface $em): Response
    {
        $question = new Contact();
        $form = $this->createForm(ContactType::class, $question);
        $form->handleRequest($request);

        $em->persist($question);
        $em->flush();

        $responseData = new InternalResponse('Dziękuję, w najbliższym czasie skontaktujemy się z Państwem.');

        try {
            if (!$form->isValid()) {
                throw new FormInvalidArgumentException(current($form->getErrors(true)));
            }

            /*$email = (new EmailSending())
                ->from($this->getParameter('email_sender'))
                ->to($this->getParameter('email_recipient'))
                ->subject('Plantia contact.')
                ->text($phoneContact->getPhone());
            $mailer->send($email);*/
        } catch (FormInvalidArgumentException $ex) {
            $responseData->error($ex->getMessage());
        } catch (\Throwable $ex) {
            $logger->error($ex->getMessage());
        }

        return new JsonResponse($responseData->toArray());
    }
}