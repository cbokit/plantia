<?php

namespace App\Controller;

use App\Util\SitemapManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class SitemapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="sitemap_index_xml")
     *
     * @param SitemapManager $manager
     *
     * @return Response
     */
    public function index(SitemapManager $manager): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/xml');

        return $this->render('sitemap/index.xml.twig', [
            'urls' => $manager->getAll(),
        ], $response);
    }
}
