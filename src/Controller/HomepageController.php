<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\GalleryImage;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     *
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function index(EntityManagerInterface $em): Response
    {
        return $this->render('homepage/index.html.twig', [
            'products'  => $em->getRepository(Product::class)->findBy([], ['priority' => 'DESC'], 6),
            'articles'  => $em->getRepository(Article::class)->findBy([], ['id' => 'DESC'],2),
            'gallery'   => $em->getRepository(GalleryImage::class)->findBy([], ['position' => 'ASC']),
            'lpProduct' => $em->getRepository(Product::class)->findOneBy([], ['price' => 'ASC']),
        ]);
    }
}
