<?php

namespace App\Controller;

use App\Entity\Realization;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class RealizationController extends AbstractController
{
    /**
     * @Route("/realizations_1233", name="realization_page")
     */
    public function index(): Response
    {
        $realizations = $this->getDoctrine()->getRepository(Realization::class)->findAll();

        return $this->render('realization/index.html.twig', [
            'realizations' => $realizations,
        ]);
    }
}
