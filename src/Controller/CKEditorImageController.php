<?php

namespace App\Controller;

use App\Util\FileNameGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CKEditorImageController extends AbstractController
{
    public const UPLOAD_PATH = 'article_content_images';

    /**
     * @Route("/ckeditor-image/browse", name="ckeditor_image_browse")
     *
     * @return Response
     */
    public function ckeditorBrowserAction(): Response
    {
        return $this->render('admin/CKEditor/browser.html.twig', [
            'images' => array_diff(scandir(self::UPLOAD_PATH), ['.', '..']),
        ]);
    }

    /**
     * @Route("cheditor-image/upload", name="ckeditor_image_upload")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function ckeditorUploadAction(Request $request): Response
    {
        $file = $request->files->get('upload');

        if(!$file || !$request->isMethod('POST')){
            throw $this->createNotFoundException();
        }

        $newName = FileNameGenerator::generate(70).'.'.$file->getClientOriginalExtension();
        $file->move(self::UPLOAD_PATH, $newName);

        return $this->render('admin/CKEditor/upload.html.twig', [
            'image' => self::UPLOAD_PATH.'/'.$newName,
        ]);
    }
}
