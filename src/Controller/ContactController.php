<?php

namespace App\Controller;

use App\Entity\Order;
use App\Exception\FormInvalidArgumentException;
use App\Form\OrderType;
use App\Form\QuestionType;
use App\Entity\Question;
use App\Model\InternalResponse;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mime\Email;

final class ContactController extends AbstractController
{
    /**
     * @Route("/send-question", name="send_qustion", methods={"POST"})
     *
     * @param Request $request
     * @param MailerInterface $mailer
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     */
    public function sendQuestion(Request $request, MailerInterface $mailer, LoggerInterface $logger, EntityManagerInterface $em): JsonResponse
    {
        $question = new Question();
        $form = $this->createForm(QuestionType::class, $question);
        $form->handleRequest($request);

        $em->persist($question);
        $em->flush();

        $responseData = new InternalResponse('Dziękuję, w najbliższym czasie skontaktujemy się z Państwem.');

        try {
            if (!$form->isValid()) {
                throw new FormInvalidArgumentException(current($form->getErrors(true)));
            }

            /*$email = (new Email())
                ->from($form->getData()->getSender())
                ->to($this->getParameter('email_recipient'))
                ->subject('Plantia question.')
                ->text($form->getData()->getContent());
            $mailer->send($email);*/
        } catch (FormInvalidArgumentException $ex) {
            $responseData->error($ex->getMessage());
        } catch (\Throwable $ex) {
            $logger->error($ex->getMessage());
        }

        return new JsonResponse($responseData->toArray());
    }

    /**
     * @Route("/send-order", name="send_order", methods={"POST"})
     *
     * @param Request $request
     * @param MailerInterface $mailer
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     */
    public function sendOrder(Request $request, MailerInterface $mailer, LoggerInterface $logger, EntityManagerInterface $em): Response
    {
        $order = new Order();
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        $em->persist($order);
        $em->flush();

        $responseData = new InternalResponse('Dziękujemy Twoje zapytanie zostało wysłane.');

        try {
            if (!$form->isValid()) {
                throw new FormInvalidArgumentException(current($form->getErrors(true)));
            }
        } catch (FormInvalidArgumentException $ex){
            $responseData->error($ex->getMessage());
        } catch (\Throwable $ex){
            $logger->error($ex->getMessage());
        }

        return new JsonResponse($responseData->toArray());
    }
}
