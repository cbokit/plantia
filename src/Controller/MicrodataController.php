<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Product;
use App\JsonLd\Builds\ArticlePageMicrodataBuild;
use App\JsonLd\Builds\ArticlesPageMicrodataBuild;
use App\JsonLd\Builds\HomePageMicrodataBuilder;
use App\JsonLd\Builds\ProductPageMicrodataBuild;
use App\JsonLd\ListItem\ItemsBuilder;
use App\JsonLd\Organization;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\JsonLd\Product as ProductMicrodataBuilder;

final class MicrodataController extends AbstractController
{
    /**
     * @param Organization $organization
     *
     * @return Response
     */
    public function renderBaseMicrodata(Organization $organization): Response
    {
        return $this->render('microdata.html.twig', [
            'microdata' => $organization->buildOrganization(),
        ]);
    }

    /**
     * @param Product $product
     * @param array $products
     * @param array $articles
     * @param HomePageMicrodataBuilder $builder
     *
     * @return Response
     */
    public function renderHomepageMicrodata(Product $product, array $products, array $articles, HomePageMicrodataBuilder $builder): Response
    {
        return $this->render('microdata.html.twig', [
            'microdata' => $builder->build($product, $products, $articles)
        ]);
    }

    /**
     * @param array $products
     * @param ItemsBuilder $itemsBuilder
     *
     * @return Response
     */
    public function renderProductsMicrodata(array $products, ItemsBuilder $itemsBuilder): Response
    {
        return $this->render('microdata.html.twig', [
            'microdata' => $itemsBuilder->buildItemList($products),
        ]);
    }

    /**
     * @param Product $product
     * @param Product[] $products
     * @param ProductPageMicrodataBuild $productMicrodataBuilder
     *
     * @return Response
     */
    public function renderProductMicrodata(Product $product, array $products, ProductPageMicrodataBuild $productMicrodataBuilder): Response
    {
        return $this->render('microdata.html.twig', [
            'microdata' => $productMicrodataBuilder->buildMicrodata($product, $products),
        ]);
    }

    /**
     * @param Article[] $articles
     * @param ArticlesPageMicrodataBuild $build
     *
     * @return Response
     */
    public function renderArticlesMicrodata(array $articles, ArticlesPageMicrodataBuild $build): Response
    {
        return $this->render('microdata.html.twig', [
            'microdata' => $build->buildMicrodata($articles),
        ]);
    }

    /**
     * @param Article $article
     * @param Article[] $articles
     * @param ArticlePageMicrodataBuild $articlesMicrodataBuild
     *
     * @return Response
     */
    public function renderArticleMicrodata(Article $article, array $articles, ArticlePageMicrodataBuild $articlesMicrodataBuild): Response
    {
        return $this->render('microdata.html.twig', [
            'microdata' => $articlesMicrodataBuild->buildMicrodata($article, $articles),
        ]);
    }
}