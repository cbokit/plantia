<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Form\OrderType;
use App\Form\QuestionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

final class RenderController extends AbstractController
{
    /**
     * @return Response
     */
    public function renderOrderForm(): Response
    {
        return $this->render('block/order_form.html.twig', [
            'form' => $this->createForm(OrderType::class)->createView(),
        ]);
    }

    /**
     * @return Response
     */
    public function renderQuestionForm(): Response
    {
        return $this->render('block/question_form.html.twig', [
            'form' => $this->createForm(QuestionType::class)->createView(),
        ]);
    }

    /**
     * @return Response
     */
    public function renderProductContactForm(): Response
    {
        return $this->render('block/product_contact_form.html.twig', [
            'form' => $this->createForm(ContactType::class)->createView(),
        ]);
    }

    /**
     * @return Response
     */
    public function renderProductsContactForm(): Response
    {
        return $this->render('block/products_contact_form.html.twig', [
            'form' => $this->createForm(ContactType::class)->createView(),
        ]);
    }

    /**
     * @return Response
     */
    public function renderBaseProductContactForm(): Response
    {
        return $this->render('block/base_product_contact_form.html.twig', [
            'form' => $this->createForm(ContactType::class)->createView(),
        ]);
    }
}
