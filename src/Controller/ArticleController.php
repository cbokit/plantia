<?php

namespace App\Controller;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ArticleController extends AbstractController
{
    /**
     * @Route("/articles", name="article_list")
     *
     * @param EntityManagerInterface $em
     * @param PaginatorInterface $paginator
     * @param Request $request
     *
     * @return Response
     */
    public function list(EntityManagerInterface $em, PaginatorInterface $paginator, Request $request): Response
    {
        $pagination = $paginator->paginate(
            $em->getRepository(Article::class)->getQListArticles(),
            $request->query->getInt('page', 1),
            30
        );

        return $this->render('article/list.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/article/{slug}-{id}", name="article_show", requirements={"slug"=".+"})
     * @ParamConverter("article", class="App:Article")
     *
     * @param string $slug
     * @param EntityManagerInterface $em
     * @param Article $article
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     */
    public function show(string $slug, Article $article, EntityManagerInterface $em): Response
    {
        if($slug !== $article->getSlug()){
            return $this->redirectToRoute('article_show', ['id' => $article->getId(), 'slug' => $article->getSlug()]);
        }

        $cache = new FilesystemAdapter();
        $currentAmount = $em->getRepository(Article::class)->getAmount();
        $cachedAmount  = $cache->get('amount_articles', function() use ($em){
            return $em->getRepository(Article::class)->getAmount();
        });

        if($cachedAmount <= $currentAmount && $currentAmount > 3){
            $randomArticles = $cache->get('random_articles', function() use ($em){
                return $em->getRepository(Article::class)->getRandomArticles();
            });
        } else {
            $randomArticles = $em->getRepository(Article::class)->getRandomArticles();
        }

        return $this->render('article/show.html.twig', [
            'article' => $article,
            'randomArticles' => $randomArticles,
        ]);
    }
}
