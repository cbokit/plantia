<?php

namespace App\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class ProductAdminController extends CRUDController
{
    /**
     * @param int $id
     *
     * @return Response
     */
    public function cloneAction(int $id): Response
    {
        try {

            $object = $this->admin->getSubject();

            if (!$object) {
                throw new NotFoundHttpException(sprintf('Unable to find the product with id: %s', $id));
            }

            $this->admin->create(clone $object);
            $this->addFlash('sonata_flash_success', 'Cloned successfully');
        } catch (\Throwable $ex){
            $this->addFlash('sonata_flash_error', $ex->getMessage());
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
        );
    }
}