<?php

namespace App\AdminBlock;

use App\Repository\ContactRepository;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Component\HttpFoundation\Response;

final class ContactBlockService extends AbstractBlockService
{
    /**
     * @var ContactRepository
     */
    private $phoneContactRepository;

    /**
     * @param $templatingOrDeprecatedName
     * @param EngineInterface|null $templating
     * @param ContactRepository $phoneContactRepository
     */
    public function __construct($templatingOrDeprecatedName, ?EngineInterface $templating, ContactRepository $phoneContactRepository)
    {
        parent::__construct($templatingOrDeprecatedName, $templating);

        $this->phoneContactRepository = $phoneContactRepository;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return void
     */
    public function configureSettings(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'message',
            'title' => 'Recent phone contacts',
            'icon' => 'fa fa-check',
            'template' => 'admin/block/recent_contacts.html.twig',
        ]);
    }

    /**
     * @param BlockContextInterface $blockContext
     * @param Response|null $response
     *
     * @return Response
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null): Response
    {
        $settings = $blockContext->getSettings();
        $recentQuestions = $this->phoneContactRepository->findBy([], ['createdAt' => 'DESC'], 8);

        return $this->renderResponse($blockContext->getTemplate(), [
            'recentContacts' => $recentQuestions,
            'block'          => $blockContext->getBlock(),
            'settings'       => $settings
        ], $response);
    }
}