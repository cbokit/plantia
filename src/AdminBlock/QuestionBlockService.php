<?php

namespace App\AdminBlock;

use App\Repository\QuestionRepository;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Component\HttpFoundation\Response;

final class QuestionBlockService extends AbstractBlockService
{
    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * @param $templatingOrDeprecatedName
     * @param EngineInterface|null $templating
     * @param QuestionRepository $questionRepository
     */
    public function __construct($templatingOrDeprecatedName, ?EngineInterface $templating, QuestionRepository $questionRepository)
    {
        parent::__construct($templatingOrDeprecatedName, $templating);

        $this->questionRepository = $questionRepository;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return void
     */
    public function configureSettings(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'message',
            'title' => 'Recent questions',
            'icon' => 'fa fa-check',
            'template' => 'admin/block/recent_questions.html.twig',
        ]);
    }

    /**
     * @param BlockContextInterface $blockContext
     * @param Response|null $response
     *
     * @return Response
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null): Response
    {
        $settings = $blockContext->getSettings();
        $recentQuestions = $this->questionRepository->findBy([], ['createdAt' => 'DESC'], 8);

        return $this->renderResponse($blockContext->getTemplate(), [
            'recentQuestions' => $recentQuestions,
            'block'           => $blockContext->getBlock(),
            'settings'        => $settings
        ], $response);
    }
}