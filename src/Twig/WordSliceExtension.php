<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

final class WordSliceExtension extends AbstractExtension
{
    /**
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('slice_word', [$this, 'sliceWord']),
        ];
    }

    /**
     * @param string|null $string
     * @param int $limit
     *
     * @return string|null
     */
    public function sliceWord(?string $string, int $limit): ?string
    {
        if(!$string){
            return $string;
        }

        if (strlen($string) > $limit){
            $string  = wordwrap($string, $limit);
            $string  = substr($string, 0, strpos($string, "\n"));
            $string .= '...';
        }

        return $string;
    }
}
