<?php

namespace App\Entity;

use App\Repository\QuestionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass=QuestionRepository::class)
 * @ORM\Table(name="questions")
 */
class Question
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Email(message="Nieprawidłowy format e-maila.")
     * @Assert\NotBlank(message="Pole e-mail jest wymagane.")
     * @ORM\Column(name="sender", type="string", length=50)
     */
    private $sender;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Pole telefon jest wymagane.")
     * @Assert\Regex("[(^-?\d+$)]", message="Pole telephone może zawierać tylko cyfry.")
     * @Assert\Length(
     *     min="8", minMessage="Pole telefonu musi zawierać więcej niż 8 znaków.",
     *     max="15", maxMessage="Numer telefonu nie istnieje."
     * )
     * @ORM\Column(name="phone", type="string", length=60)
     */
    private $phone;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Pole content jest wymagane.")
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var bool
     *
     * @Assert\NotBlank(message="Pole umowy jest wymagane.")
     */
    private $isAgree;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getSender(): ?string
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     *
     * @return self
     */
    public function setSender(string $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return self
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isAgree(): ?bool
    {
        return $this->isAgree;
    }

    /**
     * @param bool $isAgree
     *
     * @return self
     */
    public function setIsAgree(bool $isAgree): self
    {
        $this->isAgree = $isAgree;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     *
     * @return void
     */
    public function createTimestamp(): void
    {
        $this->createdAt = new \DateTime('now');
    }
}