<?php

namespace App\Entity;

use App\Repository\PropertyRepository;
use App\Util\FileNameGenerator;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="properties")
 * @ORM\Entity(repositoryClass=PropertyRepository::class)
 */
class Property
{
    /**
     * Path to uploaded files.
     */
    public const UPLOADS = 'public/uploads';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Pole 'name' jest wymagane.")
     * @Assert\Length(max="50", maxMessage="Długość pola 'name' nie może przekraczać 50 znaków.")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150)
     * @Assert\Length(
     *     min="70", minMessage="The description field should contain between 70 and 150 characters.",
     *     max="150", maxMessage="The description field should contain between 70 and 150 characters."
     * )
     */
    private $description;

    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     *
     * @return self
     */
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile|null $file
     *
     * @return self
     */
    public function setFile(?UploadedFile $file=null): self
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Manages the copying of files to the relevant place on the server.
     *
     * @throws \Exception
     */
    public function uploadImage(string $path): void
    {
        if ($this->getFile() === null) {
            return;
        }

        $newName = FileNameGenerator::generate(70).'.'.$this->getFile()->getClientOriginalExtension();
        $this->getFile()->move($path, $newName);

        $this->icon = $newName;
        $this->file = null;
    }
}