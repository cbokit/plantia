<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator as AppAssert;

/**
 * @ORM\Table(name="products")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80)
     * @Assert\NotBlank(message="Pole 'name' jest wymagane.")
     * @Assert\Length(max="80", maxMessage="Długość pola 'name' nie może przekraczać 80 znaków.")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=80)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="main_image", type="string", length=100, nullable=true)
     */
    private $mainImage;

    /**
     * @var string
     *
     * @ORM\Column(name="second_image", type="string", length=100, nullable=true)
     */
    private $secondImage;

    /**
     * @var UploadedFile
     */
    private $mainFileImage;

    /**
     * @var UploadedFile
     */
    private $secondFileImage;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Property")
     * @ORM\JoinTable(name="products_properties",
     *  joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="property_id", referencedColumnName="id")}
     * )
     */
    private $properties;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank(message="Pole 'description' jest wymagane.")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="long_description", type="text")
     * @Assert\NotBlank(message="Pole 'long description' jest wymagane.")
     * @Assert\Length(
     *     min="800", minMessage="The long description field should contain between 800 and 1500 characters.",
     *     max="1500", maxMessage="The long description field should contain between 800 and 1500 characters."
     * )
     */
    private $longDescription;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     * @Assert\NotBlank(message="Pole 'price' jest wymagane.")
     */
    private $price;

    /**
     * 1 - the most priority. 999 - the most non-priority.
     *
     * @var int
     *
     * @ORM\Column(name="priority", type="smallint")
     * @Assert\NotBlank(message="Pole 'priority' jest wymagane.")
     */
    private $priority;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProductImage", mappedBy="product", cascade={"all"}, orphanRemoval=true)
     * @AppAssert\ProductImages
     */
    private $images;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=3)
     */
    private $currency;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMainImage(): ?string
    {
        return $this->mainImage;
    }

    /**
     * @param string $mainImage
     *
     * @return self
     */
    public function setMainImage(string $mainImage): self
    {
        $this->mainImage = $mainImage;
        $this->mainFileImage = null;

        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getMainFileImage(): ?UploadedFile
    {
        return $this->mainFileImage;
    }

    /**
     * @param UploadedFile|null $mainFileImage
     *
     * @return self
     */
    public function setMainFileImage(?UploadedFile $mainFileImage=null): self
    {
        $this->mainFileImage = $mainFileImage;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSecondImage(): ?string
    {
        return $this->secondImage;
    }

    /**
     * @param string $secondImage
     *
     * @return self
     */
    public function setSecondImage(string $secondImage): self
    {
        $this->secondImage = $secondImage;

        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getSecondFileImage(): ?UploadedFile
    {
        return $this->secondFileImage;
    }

    /**
     * @param UploadedFile $secondFileImage
     *
     * @return self
     */
    public function setSecondFileImage(UploadedFile $secondFileImage): self
    {
        $this->secondFileImage = $secondFileImage;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getProperties(): ?Collection
    {
        return $this->properties;
    }

    /**
     * @param Collection|null $properties
     *
     * @return self
     */
    public function setProperties(?Collection $properties): self
    {
        $this->properties = $properties;

        return $this;
    }

    /**
     * @param Property $property
     *
     * @return self
     */
    public function addProperty(Property $property): self
    {
        if(!$this->properties->contains($property)){
            $this->properties->add($property);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getJsonProperties(): string
    {
        $data = [];

        foreach ($this->properties as $property) {
            $data[$property->getId()]['propertyName'] = $property->getName();
            $data[$property->getId()]['icon'] = '/uploads/'.$property->getIcon();
        }

        return json_encode($data);
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLongDescription(): ?string
    {
        return $this->longDescription;
    }

    /**
     * @param string $longDescription
     *
     * @return self
     */
    public function setLongDescription(string $longDescription): self
    {
        $this->longDescription = $longDescription;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return self
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     *
     * @return self
     */
    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * @param Collection $images
     *
     * @return self
     */
    public function setImages(Collection $images): self
    {
        $this->images = $images;

        return $this;
    }

    /**
     * @param ProductImage $image
     *
     * @return self
     */
    public function addImage(ProductImage $image): self
    {
        if(!$this->images->contains($image)){
            $this->images->add($image);
        }

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     *
     * @return self
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTimeInterface|null $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function createTimestamps(): void
    {
        $this->setCreatedAt(new \DateTime('now'));
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     *
     * @return self
     */
    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlicedDescription(): string
    {
        if (strlen($this->description) > 150){
            $string  = wordwrap($this->description, 150);
            $string  = substr($string, 0, strpos($string, "\n"));
            $string .= '...';

            return $string;
        }

        return $this->description;
    }
}
