<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use App\Util\FileNameGenerator;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Behat\Transliterator\Transliterator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="articles")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 */
class Article
{
    /**
     * Path to uploaded files.
     */
    public const UPLOADS = 'public/uploads';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank(message="Pole 'title' jest wymagane.")
     * @Assert\Length(max="255", maxMessage="Długość pola 'title' nie może przekraczać 255 znaków.")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank(message="Pole 'content' jest wymagane.")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="small_image", type="string", length=100, nullable=true)
     */
    private $smallImage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="og_small_image", type="string", length=100, nullable=true)
     */
    private $ogSmallImage;

    /**
     * @var string
     *
     * @ORM\Column(name="large_image", type="string", length=100, nullable=true)
     */
    private $largeImage;

    /**
     * Unmapped property to handle file uploads.
     *
     * @var UploadedFile
     */
    private $smallImgFile;

    /**
     * Unmapped property to handle file uploads.
     *
     * @var UploadedFile
     */
    private $ogSmallImgFile;

    /**
     * Unmapped property to handle file uploads.
     *
     * @var UploadedFile
     */
    private $largeImgFile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return Transliterator::transliterate($this->title ?? '');
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     *
     * @return self
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @param UploadedFile|null $file
     *
     * @return self
     */
    public function setSmallImgFile(UploadedFile $file=null) : self
    {
        $this->smallImgFile = $file;

        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getSmallImgFile() : ?UploadedFile
    {
        return $this->smallImgFile;
    }

    /**
     * @return UploadedFile|null
     */
    public function getLargeImgFile(): ?UploadedFile
    {
        return $this->largeImgFile;
    }

    /**
     * @param UploadedFile|null $file
     *
     * @return self
     */
    public function setLargeImgFile(UploadedFile $file=null): self
    {
        $this->largeImgFile = $file;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSmallImage(): ?string
    {
        return $this->smallImage;
    }

    /**
     * @param string|null $smallImage
     *
     * @return self
     */
    public function setSmallImage(?string $smallImage): self
    {
        $this->smallImage = $smallImage;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLargeImage(): ?string
    {
        return $this->largeImage;
    }

    /**
     * @param string|null $largeImage
     *
     * @return self
     */
    public function setLargeImage(?string $largeImage): self
    {
        $this->largeImage = $largeImage;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOgSmallImage(): ?string
    {
        return $this->ogSmallImage;
    }

    /**
     * @param string|null $ogSmallImage
     *
     * @return self
     */
    public function setOgSmallImage(?string $ogSmallImage): self
    {
        $this->ogSmallImage = $ogSmallImage;

        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getOgSmallImgFile(): ?UploadedFile
    {
        return $this->ogSmallImgFile;
    }

    /**
     * @param UploadedFile|null $ogSmallImgFile
     *
     * @return self
     */
    public function setOgSmallImgFile(UploadedFile $ogSmallImgFile=null): self
    {
        $this->ogSmallImgFile = $ogSmallImgFile;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     *
     * @return self
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTimeInterface $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function createTimestamps(): void
    {
        $this->setCreatedAt(new \DateTime('now'));
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));
    }

    /**
     * Manages the copying of files to the relevant place on the server.
     *
     * @throws \Exception
     */
    public function uploadImages(string $path): void
    {
        if($this->getSmallImgFile() !== null){
            $newSmallName = FileNameGenerator::generate(70).'.'.$this->getSmallImgFile()->getClientOriginalExtension();
            $this->getSmallImgFile()->move($path, $newSmallName);

            $this->smallImage   = $newSmallName;
            $this->smallImgFile = null;
        }

        if($this->getLargeImgFile() !== null){
            $newLargeName = FileNameGenerator::generate(70).'.'.$this->getLargeImgFile()->getClientOriginalExtension();
            $this->getLargeImgFile()->move($path, $newLargeName);

            $this->largeImage   = $newLargeName;
            $this->largeImgFile = null;
        }

        if($this->getOgSmallImgFile() !== null){
            $newOgSmallName = FileNameGenerator::generate(70).'.'.$this->getOgSmallImgFile()->getClientOriginalExtension();
            $this->getOgSmallImgFile()->move($path, $newOgSmallName);

            $this->ogSmallImage   = $newOgSmallName;
            $this->ogSmallImgFile = null;
        }
    }
}
