<?php

namespace App\Entity;

use App\Repository\GalleryImageRepository;
use App\Util\FileNameGenerator;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="gallery_images")
 * @ORM\Entity(repositoryClass=GalleryImageRepository::class)
 */
class GalleryImage
{
    /**
     * Path to uploaded files.
     */
    public const UPLOADS = 'public/uploads';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="smallint")
     * @Assert\NotBlank(message="Pole 'position' jest wymagane.")
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=100)
     * @Assert\NotBlank(message="Pole 'description' jest wymagane.")
     * @Assert\Length(max="100", maxMessage="Długość pola 'description' nie może przekraczać 100 znaków.")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=100, nullable=true)
     */
    private $image;

    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return self
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string $image
     *
     * @return self
     */
    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     *
     * @return self
     */
    public function setFile(UploadedFile $file): self
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Manages the copying of files to the relevant place on the server.
     *
     * @throws \Exception
     */
    public function uploadImage(string $path): void
    {
        if(null === $this->getFile()){
            return;
        }

        $newName = FileNameGenerator::generate(70).'.'.$this->getFile()->getClientOriginalExtension();
        $this->getFile()->move($path, $newName);

        $this->image = $newName;
        $this->file  = null;
    }
}
