<?php

namespace App\Entity;

use App\Repository\RealizationRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="realizations")
 * @ORM\Entity(repositoryClass=RealizationRepository::class)
 */
class Realization
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $mainImage;

    /**
     * @var UploadedFile
     */
    private $mainImageFile;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Product")
     * @ORM\JoinTable(name="realizations_products",
     *  joinColumns={@ORM\JoinColumn(name="realization_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")}
     * )
     *
     * @Assert\NotBlank(message="Pole 'products' jest wymagane.")
     */
    private $products;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Property")
     * @ORM\JoinTable(name="realizations_properties",
     *  joinColumns={@ORM\JoinColumn(name="realization_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="property_id", referencedColumnName="id")}
     * )
     */
    private $properties;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getMainImage(): ?string
    {
        return $this->mainImage;
    }

    /**
     * @param string $mainImage
     * 
     * @return self
     */
    public function setMainImage(string $mainImage): self
    {
        $this->mainImage     = $mainImage;
        $this->mainImageFile = null;

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getProducts(): ?Collection
    {
        return $this->products;
    }

    /**
     * @param Collection $products
     *
     * @return self
     */
    public function setProducts(Collection $products): self
    {
        $this->products = $products;

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getProperties(): ?Collection
    {
        return $this->properties;
    }

    /**
     * @param Collection $properties
     *
     * @return self
     */
    public function setProperties(Collection $properties): self
    {
        $this->properties = $properties;

        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getMainImageFile(): ?UploadedFile
    {
        return $this->mainImageFile;
    }

    /**
     * @param UploadedFile $mainImageFile
     *
     * @return self
     */
    public function setMainImageFile(UploadedFile $mainImageFile): self
    {
        $this->mainImageFile = $mainImageFile;

        return $this;
    }
}
