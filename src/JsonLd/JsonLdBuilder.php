<?php

namespace App\JsonLd;

use App\Entity\Article;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class JsonLdBuilder
{
    public const SCHEMA = '';

    /**
     * @var OrganizationConfModel
     */
    private $organizationModel;

    /**
     * @var string
     */
    private $host;

    /**
     * @param OrganizationConfModel $organization
     * @param UrlGeneratorInterface $route
     */
    public function __construct(OrganizationConfModel $organization, UrlGeneratorInterface $route)
    {
        $this->organizationModel = $organization;
        $this->host = sprintf('%s://%s', $route->getContext()->getScheme(), $route->getContext()->getHost());
    }

    /**
     * @return string
     */
    public function createBaseOrganization(): string
    {
        $microdata = [
            "@context" => 'http://schema.org',
            "@type" => "Organization",
            "@id" => $this->organizationModel->getUrl(),
            "url" => $this->organizationModel->getUrl(),
            "logo"=> $this->organizationModel->getUrl().'/img/'.'logo_black.svg',
            "address" => [
                "@type" => "PostalAddress",
                "addressLocality" => $this->organizationModel->getCity(),
                "postalCode" => $this->organizationModel->getPostalCode(),
                "streetAddress" => $this->organizationModel->getStreet(),
            ],
            "email" => $this->organizationModel->getEmail(),
            "name"  => $this->organizationModel->getName(),
            "telephone" => $this->organizationModel->getPhone(),
            "sameAs" => [
                $this->organizationModel->getFacebook(),
            ],
        ];

        return $this->decodeResult($microdata);
    }

    /**
     * @return string
     */
    public function createOrganization(): string
    {
        $microdata = [
            [
                "@context" => self::SCHEMA,
                "@type" => "Organization",
                "@id" => $this->organizationModel->getUrl(),
                "url" => $this->organizationModel->getUrl(),
                "logo"=> $this->organizationModel->getUrl().'/img/'.'logo_black.svg',
                "address" => [
                    "@type" => "PostalAddress",
                    "addressLocality" => $this->organizationModel->getCity(),
                    "postalCode" => $this->organizationModel->getPostalCode(),
                    "streetAddress" => $this->organizationModel->getStreet(),
                ],
                "email" => $this->organizationModel->getEmail(),
                "name"  => $this->organizationModel->getName(),
                "telephone" => $this->organizationModel->getPhone(),
            ],
            [
                "@context" => self::SCHEMA,
                "@type" => "Organization",
                "sameAs" => [
                    "facebook" => $this->organizationModel->getFacebook(),
                ],
            ],
            [
                "@context" => self::SCHEMA,
                "@type" => "BreadcrumbList",
                "itemListElement" => [
                    "@type" => "ListItem",
                    "position" => "1",
                    "item" => [
                        "@type" => "Thing",
                        "name" => "Start",
                        "@id" => $this->organizationModel->getUrl(),
                    ]
                ]
            ],
        ];
        return html_entity_decode(json_encode($microdata, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    }

    /**
     * @param string $link
     *
     * @return string
     */
    public function createArticles(string $link): string
    {
        $microdata = [
            [
                "@context" => self::SCHEMA,
                "@type" => "Organization",
                "url" => $this->organizationModel->getUrl(),
                "logo"=> $this->organizationModel->getUrl().'/img/'.'logo_black.svg',
            ],
            [
                "@context" => self::SCHEMA,
                "@type" => "Organization",
                "sameAs" => [
                    "facebook" => $this->organizationModel->getFacebook(),
                ],
            ],
            [
                "@context" => self::SCHEMA,
                "@type" => "WebSite",
                "name" => $this->organizationModel->getDomain(),
                "url" => $this->organizationModel->getUrl() . $link,
            ],
        ];
        return html_entity_decode(json_encode($microdata, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    }

    /**
     * @param Article $article
     * @param string $link
     * @param string $articleLink
     *
     * @return string
     */
    public function createArticle(Article $article, string $link, string $articleLink): string
    {
        $content = new Crawler($article->getContent());

        $microdata = [
            [
                '@context' => self::SCHEMA,
                "@type" => "Article",
                "name" => $article->getTitle(),
                "headline" => $article->getTitle(),
                "description" => $content->text(),
                "datePublished" => $article->getCreatedAt()->format("Y-m-d"),
                "mainEntityOfPage" => [
                    "@type" => "WebPage",
                    "@id" => "https://google.com/article",
                ],
                "publisher" => [
                    "@type" => "Organization",
                    "name" => $this->organizationModel->getDomain(),
                ],
                "image" => [
                    "@type" => "ImageObject",
                    "url" => $this->organizationModel->getUrl() .'/'. $article->getLargeImage(),
                ],
                "author" => [
                    "@type" => "Organization",
                    "name" => $this->organizationModel->getDomain(),
                ],
            ],
            [
                "@context" => self::SCHEMA,
                "@type" => "Organization",
                "url" => $this->organizationModel->getUrl(),
                "logo"=> $this->organizationModel->getUrl().'/img/'.'logo_black.svg',
            ],
            [
                "@context" => self::SCHEMA,
                "@type" => "Organization",
                "sameAs" => [
                    "facebook" => $this->organizationModel->getFacebook(),
                ],
            ],
            [
                "@context" => self::SCHEMA,
                "@type" => "WebSite",
                "name" => $this->organizationModel->getDomain(),
                "url" => $this->organizationModel->getUrl() . $articleLink,
            ],
            [
                "@context" => self::SCHEMA,
                "@type" => "BreadcrumbList",
                "itemListElement" => [
                    [
                        "@type" => "ListItem",
                        "position" => "1",
                        "item" => [
                            "@type" => "Thing",
                            "name" => "Start",
                            "@id" => $this->organizationModel->getUrl(),
                        ],
                    ],
                    [
                        "@type" => "ListItem",
                        "position" => "2",
                        "item" => [
                            "@type" => "Thing",
                            "name" => "Artykuły",
                            "@id" => $this->organizationModel->getUrl() . $link,
                        ],
                    ],
                ],
            ],
        ];
        return html_entity_decode(json_encode($microdata, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    }



    /**
     * @return string
     */
    public function createWebSite(): string
    {
        $microdata = [
            '@context' => self::SCHEMA,
            '@type' => 'WebSite',
            'name' => $this->organizationModel->getBrand(),
            'url' => $this->host,
            'sameAs' => [
                $this->organizationModel->getFacebook(),
            ],
        ];

        return $this->decodeResult($microdata);
    }

    public function createHomePage(): string
    {
        $microdata = [

        ];
    }

    /**
     * @param array $microdata
     *
     * @return string
     */
    private function decodeResult(array $microdata): string
    {
        return html_entity_decode(json_encode($microdata, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    }
}