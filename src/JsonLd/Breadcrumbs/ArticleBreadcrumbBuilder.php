<?php

namespace App\JsonLd\Breadcrumbs;

use App\Entity\Article;
use App\JsonLd\Utils\Helper;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ArticleBreadcrumbBuilder
{
    /**
     * @var UrlGeneratorInterface
     */
    private $route;

    /**
     * @var string
     */
    private $host;

    /**
     * @param UrlGeneratorInterface $route
     */
    public function __construct(UrlGeneratorInterface $route)
    {
        $this->route = $route;

        $this->host = sprintf('%s://%s', $this->route->getContext()->getScheme(), $route->getContext()->getHost());
    }

    /**
     * @param Article $article
     *
     * @return string
     */
    public function buildArticleBreadcrumbs(Article $article): string
    {
        $microdata = [
            '@context' => 'http://schema.org',
            '@type' => 'BreadcrumbList',
            'itemListElement' => [
                [
                    '@type' => 'ListItem',
                    'position' => 1,
                    'name' => 'Strona główna',
                    'url' => sprintf('%s%s', $this->host, $this->route->generate('home_page')),
                ],
                [
                    '@type' => 'ListItem',
                    'position' => 2,
                    'name' => 'Artykuły',
                    'url' => sprintf('%s%s', $this->host, $this->route->generate('article_list')),
                ],
                [
                    '@type' => 'ListItem',
                    'position' => 3,
                    'name' => $article->getTitle(),
                    'url' => sprintf('%s%s', $this->host, $this->route->generate('article_show', [
                        'id' => $article->getId(), 'slug' => $article->getSlug(),
                    ])),
                ],
            ],
        ];

        return Helper::decodeResult($microdata);
    }

    /**
     * @return string
     */
    public function buildArticlesBreadcrumbs(): string
    {
        $microdata = [
            '@context' => 'http://schema.org',
            '@type' => 'BreadcrumbList',
            'itemListElement' => [
                [
                    '@type' => 'ListItem',
                    'position' => 1,
                    'name' => 'Strona główna',
                    'url' => sprintf('%s%s', $this->host, $this->route->generate('home_page')),
                ],
                [
                    '@type' => 'ListItem',
                    'position' => 2,
                    'name' => 'Artykuły',
                    'url' => sprintf('%s%s', $this->host, $this->route->generate('article_list')),
                ],
            ]
        ];

        return Helper::decodeResult($microdata);
    }
}