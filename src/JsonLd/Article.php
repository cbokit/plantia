<?php

namespace App\JsonLd;

use App\Entity\Article as ArticleEntity;
use App\JsonLd\Utils\Helper;
use App\Util\TextUtil;
use Symfony\Component\DomCrawler\Crawler;

final class Article extends BaseMicrodata
{
    /**
     * @param ArticleEntity $article
     *
     * @return string
     */
    public function buildArticle(ArticleEntity $article): string
    {
        $content = new Crawler($article->getContent());
        $path = $this->route->generate('article_show', [
            'id' => $article->getId(), 'slug' => $article->getSlug()
        ]);

        $microdata = [
            '@context' => self::SCHEMA,
            "@type" => "Article",
            "name" => $article->getTitle(),
            "headline" => $article->getTitle(),
            "description" => TextUtil::sliceWord($content->text(), 150),
            "datePublished" => $article->getCreatedAt()->format("Y-m-d"),
            'url' => sprintf('%s%s', $this->host, $path),
            'image' => [
                '@type' => 'ImageObject',
                'url' => sprintf('%s/uploads/%s', $this->host, $article->getLargeImage())
            ],
            "publisher" => [
                "@type" => "Organization",
                "name" => $this->organizationModel->getBrand(),
            ],
            "author" => [
                "@type" => "Organization",
                "name" => $this->organizationModel->getBrand(),
            ],
        ];

        return Helper::decodeResult($microdata);
    }
}