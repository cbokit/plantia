<?php

namespace App\JsonLd;


use App\JsonLd\Utils\Helper;

final class Organization extends BaseMicrodata
{
    /**
     * @return string
     */
    public function buildOrganization(): string
    {
        $microdata = [
            "@context" => self::SCHEMA,
            "@type" => "Organization",
            "@id" => $this->host,
            "url" => $this->host,
            "logo"=> sprintf('%s/img/logo_black.svg', $this->host),
            "address" => [
                "@type" => "PostalAddress",
                "addressLocality" => $this->organizationModel->getCity(),
                "postalCode" => $this->organizationModel->getPostalCode(),
                "streetAddress" => $this->organizationModel->getStreet(),
            ],
            "email" => $this->organizationModel->getEmail(),
            "name"  => $this->organizationModel->getName(),
            "telephone" => $this->organizationModel->getPhone(),
            "sameAs" => [
                $this->organizationModel->getFacebook(),
            ],
        ];

        return Helper::decodeResult($microdata);
    }
}