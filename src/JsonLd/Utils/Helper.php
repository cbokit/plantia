<?php

namespace App\JsonLd\Utils;

final class Helper
{
    /**
     * @param array $microdata
     *
     * @return string
     */
    public static function decodeResult(array $microdata): string
    {
        return html_entity_decode(json_encode($microdata, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    }
}