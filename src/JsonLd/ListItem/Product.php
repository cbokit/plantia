<?php

namespace App\JsonLd\ListItem;

use App\Entity\Product as ProductEntity;

final class Product extends AbstractItemBuilder
{
    /**
     * @inheritDoc
     */
    public function buildItem($obj, int $position): array
    {
        if(!$obj instanceof ProductEntity){
            throw new \LogicException('Current object is not a product.');
        }

        $path = $this->route->generate('show_product', ['id' => $obj->getId(), 'slug' => $obj->getSlug()]);

        return [
            '@type' => 'ListItem',
            'position' => $position,
            'item' => [
                '@type' => 'Product',
                'name' => $obj->getName(),
                'image' => sprintf('%s/uploads/%s', $this->host, $obj->getMainImage()),
                'url' => sprintf('%s%s', $this->host, $path),
                'description' => $obj->getDescription(),
                'brand' => [
                    '@type' => 'Thing',
                    'name' => $this->confModel->getBrand(),
                ],
                'offers' => [
                    'priceCurrency' => $obj->getCurrency(),
                    'price' => $obj->getPrice(),
                    'priceValidUntil' => (new \DateTime('now'))->format('Y-m-d'),
                    'itemCondition' => 'http://schema.org/UsedCondition',
                    'availability' => 'http://schema.org/InStock',
                    'seller' => [
                        '@type' => 'Organization',
                        'name' => $this->confModel->getName(),
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getItemName(): string
    {
        return ItemsBuilder::TYPE_PRODUCT;
    }
}