<?php

namespace App\JsonLd\ListItem;

use App\Entity\Product as ProductEntity;
use App\Entity\Article as ArticleEntity;
use App\JsonLd\Utils\Helper;

final class ItemsBuilder
{
    public const TYPE_PRODUCT = 'Product';
    public const TYPE_ARTICLE = 'Article';

    /**
     * @var iterable|AbstractItemBuilder[]
     */
    private $itmBuilders;

    /**
     * @param iterable $itmBuilders
     */
    public function __construct(iterable $itmBuilders)
    {
        $this->itmBuilders = $itmBuilders;
    }

    /**
     * @param array $objects
     * @param string|null $type
     *
     * @return string
     */
    public function buildItemList(array $objects, string $type = null): string
    {
        $microdata = [
            '@context' => 'http://schema.org',
            '@type' => 'ItemList',
            'itemListElement' => [],
        ];

        if($type){
            $this->addTypeFields($type, $microdata);
        }

        foreach ($objects as $position => $object) {
            switch (true){
                case $object instanceof ProductEntity:
                    $itemBuilder = $this->getItemBuilder(self::TYPE_PRODUCT);
                    break;
                case $object instanceof ArticleEntity:
                    $itemBuilder = $this->getItemBuilder(self::TYPE_ARTICLE);
                    break;
                default:
                    throw new \LogicException(sprintf('Class %s does not support microdata', get_class($object)));
            }

            $microdata['itemListElement'][] = $itemBuilder->buildItem($object, $position + 1);
        }

        return Helper::decodeResult($microdata);
    }

    /**
     * @param string $type
     * @param string[] $microdata
     *
     * @return void
     */
    private function addTypeFields(string $type, array &$microdata): void
    {
        switch ($type){
            case self::TYPE_ARTICLE:
                $microdata['name'] = 'Artykuły';
                $microdata['description'] = 'Plantia - Artykuły';
                break;
            case self::TYPE_PRODUCT:
                $microdata['name'] = 'Produkty';
                $microdata['description'] = 'Plantia - Produkty';
                break;
        }
    }

    /**
     * @param string $type
     *
     * @return AbstractItemBuilder
     */
    private function getItemBuilder(string $type): AbstractItemBuilder
    {
        foreach ($this->itmBuilders as $itmBuilder) {
            if($itmBuilder->getItemName() === $type){
                return $itmBuilder;
            }
        }

        throw new \LogicException(sprintf('Item %s is not defined.', $type));
    }
}