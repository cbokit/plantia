<?php

namespace App\JsonLd\ListItem;

use App\JsonLd\OrganizationConfModel;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class AbstractItemBuilder
{
    /**
     * @var UrlGeneratorInterface
     */
    protected $route;

    /**
     * @var OrganizationConfModel
     */
    protected $confModel;

    /**
     * @var string
     */
    protected $host;

    public function __construct(UrlGeneratorInterface $route, OrganizationConfModel $confModel)
    {
        $this->route = $route;
        $this->confModel = $confModel;

        $this->host = sprintf('%s://%s', $this->route->getContext()->getScheme(), $route->getContext()->getHost());
    }

    /**
     * @param mixed $obj
     * @param int $position
     *
     * @return array
     */
    abstract public function buildItem($obj, int $position): array;

    /**
     * @return string
     */
    abstract public function getItemName(): string;
}