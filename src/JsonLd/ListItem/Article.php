<?php

namespace App\JsonLd\ListItem;

use App\Entity\Article as ArticleEntity;
use App\Util\TextUtil;
use Symfony\Component\DomCrawler\Crawler;

final class Article extends AbstractItemBuilder
{
    /**
     * @inheritDoc
     */
    public function buildItem($obj, int $position): array
    {
        if(!$obj instanceof ArticleEntity){
            throw new \LogicException('Current object is not an article.');
        }

        $path = $this->route->generate('article_show', ['id' => $obj->getId(), 'slug' => $obj->getSlug()]);
        $content = new Crawler($obj->getContent());

        return [
            '@type' => 'ListItem',
            'position' => $position,
            'item' => [
                "@type" => "Article",
                "name" => $obj->getTitle(),
                "headline" => $obj->getTitle(),
                "description" => TextUtil::sliceWord($content->text(), 150),
                "datePublished" => $obj->getCreatedAt()->format("Y-m-d"),
                'url' => sprintf('%s%s', $this->host, $path),
                'image' => [
                    '@type' => 'ImageObject',
                    'url' => sprintf('%s/uploads/%s', $this->host, $obj->getLargeImage()),
                ],
                "publisher" => [
                    "@type" => "Organization",
                    "name" => $this->confModel->getBrand(),
                    "url" => $this->host,
                ],
                "author" => [
                    "@type" => "Organization",
                    "name" => $this->confModel->getBrand(),
                ],
            ]
        ];
    }

    /**
     * @inheritDoc
     */
    public function getItemName(): string
    {
        return ItemsBuilder::TYPE_ARTICLE;
    }
}