<?php

namespace App\JsonLd\Builds;

use App\Entity\Article;
use App\Entity\Product;
use App\JsonLd\ListItem\ItemsBuilder;
use App\JsonLd\Product as ProductMicrodata;

final class HomePageMicrodataBuilder
{
    /**
     * @var ItemsBuilder
     */
    private $itemList;

    /**
     * @var ProductMicrodata
     */
    private $productMicrodata;

    /**
     * @param ItemsBuilder $itemList
     * @param ProductMicrodata $productMicrodata
     */
    public function __construct(ItemsBuilder $itemList, ProductMicrodata $productMicrodata)
    {
        $this->itemList = $itemList;
        $this->productMicrodata = $productMicrodata;
    }

    /**
     * @param Product $product
     * @param Product[] $products
     * @param Article[] $articles
     *
     * @return array
     */
    public function build(Product $product, array $products, array $articles): array
    {
        return [
            $this->itemList->buildItemList(array_merge($products, $articles)),
            $this->productMicrodata->buildProduct($product),
        ];
    }
}