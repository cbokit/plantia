<?php

namespace App\JsonLd\Builds;

use App\Entity\Article;
use App\JsonLd\Breadcrumbs\ArticleBreadcrumbBuilder;
use App\JsonLd\ListItem\ItemsBuilder;

final class ArticlesPageMicrodataBuild
{
    private $listItemBuilder;
    private $articleBreadcrumbBuilder;

    /**
     * @param ItemsBuilder $listItemBuilder
     * @param ArticleBreadcrumbBuilder $articleBreadcrumbBuilder
     */
    public function __construct(ItemsBuilder $listItemBuilder, ArticleBreadcrumbBuilder $articleBreadcrumbBuilder)
    {
        $this->listItemBuilder = $listItemBuilder;
        $this->articleBreadcrumbBuilder = $articleBreadcrumbBuilder;
    }

    /**
     * @param Article[] $articles
     *
     * @return array
     */
    public function buildMicrodata(array $articles): array
    {
        return [
            $this->articleBreadcrumbBuilder->buildArticlesBreadcrumbs(),
            $this->listItemBuilder->buildItemList($articles, ItemsBuilder::TYPE_ARTICLE),
        ];
    }
}