<?php

namespace App\JsonLd\Builds;

use App\Entity\Article;
use App\JsonLd\Article as ArticleBuilder;
use App\JsonLd\Breadcrumbs\ArticleBreadcrumbBuilder;
use App\JsonLd\ListItem\ItemsBuilder;

final class ArticlePageMicrodataBuild
{
    private $articleBuilder;
    private $listItemBuilder;
    private $articleBreadcrumbBuilder;

    /**
     * @param ArticleBuilder $articleBuilder
     * @param ItemsBuilder $listItemBuilder
     * @param ArticleBreadcrumbBuilder $articleBreadcrumbBuilder
     */
    public function __construct(ArticleBuilder $articleBuilder, ItemsBuilder $listItemBuilder, ArticleBreadcrumbBuilder $articleBreadcrumbBuilder)
    {
        $this->articleBuilder = $articleBuilder;
        $this->listItemBuilder = $listItemBuilder;
        $this->articleBreadcrumbBuilder = $articleBreadcrumbBuilder;
    }

    /**
     * @param Article $article
     * @param Article[] $articles
     *
     * @return array
     */
    public function buildMicrodata(Article $article, array $articles): array
    {
        return [
            $this->articleBreadcrumbBuilder->buildArticleBreadcrumbs($article),
            $this->listItemBuilder->buildItemList($articles, ItemsBuilder::TYPE_ARTICLE),
            $this->articleBuilder->buildArticle($article),
        ];
    }
}