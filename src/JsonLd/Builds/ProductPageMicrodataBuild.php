<?php

namespace App\JsonLd\Builds;

use App\Entity\Product;
use App\JsonLd\ListItem\ItemsBuilder;
use App\JsonLd\Product as ProductBuilder;

final class ProductPageMicrodataBuild
{
    private $productBuilder;
    private $listItemBuilder;

    /**
     * @param ProductBuilder $productBuilder
     * @param ItemsBuilder $listItemBuilder
     */
    public function __construct(ProductBuilder $productBuilder, ItemsBuilder $listItemBuilder)
    {
        $this->productBuilder = $productBuilder;
        $this->listItemBuilder = $listItemBuilder;
    }

    /**
     * @param Product $product
     * @param Product[] $products
     * @return array
     */
    public function buildMicrodata(Product $product, array $products): array
    {
        return [
            $this->listItemBuilder->buildItemList($products, ItemsBuilder::TYPE_PRODUCT),
            $this->productBuilder->buildProduct($product),
        ];
    }
}