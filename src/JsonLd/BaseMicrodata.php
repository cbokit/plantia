<?php

namespace App\JsonLd;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BaseMicrodata
{
    public const SCHEMA = 'http://schema.org';

    /**
     * @var OrganizationConfModel
     */
    protected $organizationModel;

    /**
     * @var string
     */
    protected $host;

    /**
     * @var UrlGeneratorInterface
     */
    protected $route;

    /**
     * @param OrganizationConfModel $organization
     * @param UrlGeneratorInterface $route
     */
    public function __construct(OrganizationConfModel $organization, UrlGeneratorInterface $route)
    {
        $this->organizationModel = $organization;
        $this->route = $route;

        $this->host = sprintf('%s://%s', $route->getContext()->getScheme(), $route->getContext()->getHost());
    }
}