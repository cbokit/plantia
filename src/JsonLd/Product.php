<?php

namespace App\JsonLd;

use App\Entity\Product as ProductEntity;
use App\JsonLd\Utils\Helper;

final class Product extends BaseMicrodata
{
    /**
     * @param ProductEntity $product
     *
     * @return string
     */
    public function buildProduct(ProductEntity $product): string
    {
        $microdata = [
            '@context' => self::SCHEMA,
            '@type' => 'Product',
            'name' => $product->getName(),
            'image' => sprintf('%s/uploads/%s', $this->host, $product->getMainImage()),
            'description' => $product->getDescription(),
            'brand' => [
                '@type' => 'Thing',
                'name' => $this->organizationModel->getBrand(),
            ],
            'offers' => [
                '@type' => 'Offer',
                'priceCurrency' => $product->getCurrency(),
                'price' => $product->getPrice(),
                'priceValidUntil' => (new \DateTime('now'))->format('Y-m-d'),
                'itemCondition' => 'http://schema.org/UsedCondition',
                'availability' => 'http://schema.org/InStock',
                'seller' => [
                    '@type' => 'Organization',
                    'name' => $this->organizationModel->getName(),
                ],
            ],
        ];

        return Helper::decodeResult($microdata);
    }
}