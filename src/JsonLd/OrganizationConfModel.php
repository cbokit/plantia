<?php

namespace App\JsonLd;

final class OrganizationConfModel
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $postalCode;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $facebook;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var string
     */
    private $brand;

    /**
     * @param array $conf
     */
    public function __construct(array $conf)
    {
        $this->name = $conf['name'];
        $this->url = $conf['url'];
        $this->street = $conf['street'];
        $this->city = $conf['city'];
        $this->postalCode = $conf['postalCode'];
        $this->email = $conf['email'];
        $this->phone = $conf['phone'];
        $this->facebook = $conf['facebook'];
        $this->domain = $conf['domain'];
        $this->brand = $conf['brand'];
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getFacebook(): string
    {
        return $this->facebook;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return ucfirst($this->brand);
    }
}