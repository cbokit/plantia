<?php

namespace App\Validator;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class ProductImagesValidator extends ConstraintValidator
{
    private const WIDTH  = 1440;
    private const HEIGHT = 1920;

    private const UPLOADED_NUMBER = 6;

    /**
     * @param Collection $value
     * @param ProductImages $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if(!$value instanceof Collection){
            return;
        }

        if(count($value) !== self::UPLOADED_NUMBER){
            $this->context
                ->buildViolation($constraint->totalUploadedMessage)
                ->setParameter('{{ value }}', count($value))
                ->addViolation();
        }

        foreach ($value as $image){
            if($image->getFile() instanceof UploadedFile){
                list($width, $height) = getimagesize($image->getFile());

                if((int)$width !== self::WIDTH || (int)$height !== self::HEIGHT){
                    $this->context
                        ->buildViolation($constraint->resolutionMessage)
                        ->setParameter('{{ value }}', $image->getPosition())
                        ->setParameter('{{ width }}', $width)
                        ->setParameter('{{ height }}', $height)
                        ->addViolation();
                }
            }
        }
    }
}