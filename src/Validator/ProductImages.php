<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
final class ProductImages extends Constraint
{
    public $resolutionMessage = 'Resolution of the image (with position "{{ value }}") should be 1440x1920. Current resolution is {{ width }}x{{ height }}';
    public $totalUploadedMessage = 'The number of uploaded images should be 6. Current number is {{ value }}';
}