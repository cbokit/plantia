<?php

namespace App\Model;

final class InternalResponse
{
    public const SUCCESS = 1;
    public const FAIL = 2;

    /**
     * @var int
     */
    private $code;

    /**
     * @var string
     */
    private $message;

    /**
     * @param string $message
     * @param int    $code
     */
    public function __construct(string $message, int $code = self::SUCCESS)
    {
        $this->message = $message;
        $this->code    = $code;
    }

    /**
     * @param string $message
     *
     * @return void
     */
    public function error(string $message): void
    {
        $this->message = $message;
        $this->code    = self::FAIL;
    }

    /**
     * @return string[]
     */
    public function toArray(): array
    {
        return [
            'code' => $this->code,
            'message' => $this->message,
        ];
    }
}