import { errorToast, successToast } from "./toast";

export class InternalResponseHandler {
    static success = 1;
    static fail    = 2;

    static showHandledMessage(response) {
        switch (response.code){
            case this.success:
                successToast(response.message);
                return;
            case this.fail:
                errorToast(response.message);
                return;
            default:
                errorToast('Usługa jest chwilowo niedostępna.');
                return;
        }
    }
}