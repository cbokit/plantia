import validation from 'popup-validation/src';

export class Validator {
    static initForm(formSelector, events = ["change", "paste"]){
        validation.init(formSelector,{
            events: events
        });

        return validation;
    }

    static addIntegerRule(formObj, message){
        formObj.rules['integer'] = {
            message: message,
            method: el => {
                return el.value === "" || /^-?\d+$/.test(el.value);
            }
        }
    }

    static addMinLengthRule(formObj, message){
        formObj.rules['min'] = {
            message: message,
            method: el => {
                return el.value.length >= 9 || !/^-?\d+$/.test(el.value);
            }
        }
    }

    static addMaxLengthRule(formObj, message){
        formObj.rules['max'] = {
            message: message,
            method: el => {
                return el.value.length <= 15 || !/^-?\d+$/.test(el.value);
            }
        }
    }

    static addEmailPlRule(formObj, message){
        formObj.rules['emailPl'] = {
            message: message,
            method: el => {
                return el.value === "" || /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(el.value);
            }
        }
    }

    static addRequiredPlRule(formObj, message){
        formObj.rules['requiredPl'] = {
            message: message,
            method: el => {
                return el.value !== "";
            }
        }
    }

    static isValidForm(formSelector){
        return validation.isValid(formSelector);
    }
}