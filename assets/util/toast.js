import iziToast from 'izitoast';

/**
 * @param title
 * @param timeout
 * @param position
 */
export function successToast(title, timeout = 6000, position = 'bottomRight'){
    iziToast.success({
        title: title,
        timeout: timeout,
        position: position,
        displayMode: 'once',
        closeOnEscape: true,
    });
}

/**
 * @param title
 * @param timeout
 * @param position
 */
export function errorToast(title, timeout = 6000, position = 'bottomRight'){
    iziToast.error({
        title: title,
        timeout: timeout,
        position: position,
        displayMode: 'once',
        closeOnEscape: true
    });
}