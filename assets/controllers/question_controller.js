import { Controller } from '@hotwired/stimulus';
import { Validator } from '../util/Validator';
import { errorToast } from "../util/toast";
import { InternalResponseHandler } from '../util/InternalResponseHandler';

export default class extends Controller
{
    connect() {
        const form = Validator.initForm('form[name="question"]');

        Validator.addIntegerRule(form, 'Pole może zawierać tylko cyfry');
        Validator.addMinLengthRule(form, 'Pole musi zawierać więcej niż 9 znaków');
    }

    send(e){
        let input = $(e.target).parent('form').find('.custom_check input[type="checkbox"]');

        if(!input.is(':checked')){
            input.addClass('validate-error')
                 .addClass('is-invalid');
            errorToast('Wszystkie pola formularza muszą być wypełnione.');
        } else {
            input.removeClass('validate-error')
                 .removeClass('is-invalid');

            e.preventDefault();
            e.stopImmediatePropagation();

            if(Validator.isValidForm('form[name="question"]')) {
                const target = $(e.target);
                target.after('<div class="full-screen-loader"></div>');

                $.ajax({
                    url: target.data('ref'),
                    method: 'POST',
                    dataType: 'json',
                    data: target.parents('form').serialize(),
                    success: function (response){
                        target.siblings('.full-screen-loader').remove();
                        InternalResponseHandler.showHandledMessage(response);

                        if(response.status === InternalResponseHandler.success){
                            target.parents('form').trigger('reset');
                        }
                    },
                    error: function (){
                        target.siblings('.full-screen-loader').remove();
                        errorToast('Usługa jest chwilowo niedostępna.');
                    }
                });
            } else {
                errorToast('Wszystkie pola formularza muszą być wypełnione.');
            }
        }
    }

    accept(e){
        let input = $(e.target).find('input[type="checkbox"]');

        if(!input.is(':checked')){
            input.addClass('validate-error')
                 .addClass('is-invalid');
        } else {
            input.removeClass('validate-error')
                 .removeClass('is-invalid');
        }
    }
}