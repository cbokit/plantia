import { Controller } from '@hotwired/stimulus';
import {InternalResponseHandler} from "../util/InternalResponseHandler";
import {errorToast} from "../util/toast";

export default class extends Controller {
    buildCache(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        const target = $(e.target);
        target.after('<div class="full-screen-loader"></div>');
        $.ajax({
            url: target.data('ref'),
            method: 'POST',
            dataType: 'json',
            success: function (response){
                target.siblings('.full-screen-loader').remove();
                InternalResponseHandler.showHandledMessage(response);
            },
            error: function (){
                target.siblings('.full-screen-loader').remove();
                errorToast('Usługa jest chwilowo niedostępna.');
            }
        });
    }
}