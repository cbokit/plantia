import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
    connect(){
        window.addEventListener('scroll', function() {
            let block = document.querySelector('.phone_info_fixed');
            let rectStart = document.querySelector('section.hero').getBoundingClientRect();
            let rectEnd = document.querySelector('#kontakt').getBoundingClientRect();

            if (rectStart.bottom + 1 <= window.innerHeight) {
                block.style.display = 'block';
            } else {
                block.style.display = 'none';
            }

            if(rectEnd.top + 210 <= window.innerHeight){
                block.style.display = 'none';
            }
        });
    }
}