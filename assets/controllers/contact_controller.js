import { Controller } from '@hotwired/stimulus';
import {Validator} from "../util/Validator";
import {InternalResponseHandler} from "../util/InternalResponseHandler";
import {errorToast} from "../util/toast";

export default class extends Controller {
    static targets = ['form'];

    connect() {
        const form = Validator.initForm('form[name="products_contact"]');

        Validator.addIntegerRule(form, 'Ilość musi się składać tylko z cyfr.');
        Validator.addRequiredPlRule(form, 'Pole nie może być puste.');
        Validator.addMinLengthRule(form, 'Pole musi zawierać więcej niż 9 znaków');
    }

    sendContact(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        const name = $(e.target).parents('form').data('name');

        if(Validator.isValidForm('form[data-name="'+name+'"]')) {
            const target = $(e.target);
            target.after('<div class="full-screen-loader"></div>');

            $.ajax({
                url: target.data('ref'),
                method: 'POST',
                dataType: 'json',
                data: $(e.target).parents('form').serialize(),
                success: function (response){
                    target.siblings('.full-screen-loader').remove();
                    InternalResponseHandler.showHandledMessage(response);

                    if(response.code === InternalResponseHandler.success){
                        $(e.target).parents('form').trigger('reset');
                    }
                },
                error: function (){
                    target.siblings('.full-screen-loader').remove();
                    errorToast('Usługa jest chwilowo niedostępna.');
                }
            });
        } else {
            errorToast('Wszystkie pola formularza muszą być wypełnione.');
        }
    }
}