import { Controller } from '@hotwired/stimulus';
import {Validator} from "../util/Validator";
import {InternalResponseHandler} from "../util/InternalResponseHandler";
import {errorToast} from "../util/toast";

export default class extends Controller {
    static targets = ['image', 'products'];

    connect() {
        const defElement = $(this.productsTarget).find(':first-child');

        $(this.imageTarget).attr('src', defElement.data('img'));
        $(this.imageTarget).attr('alt', defElement.text());


        const form = Validator.initForm('form[name="order"]');

        Validator.addIntegerRule(form, 'Ilość musi się składać tylko z cyfr.');
        Validator.addEmailPlRule(form, 'Niepoprawny adres email.');
        Validator.addRequiredPlRule(form, 'Pole nie może być puste.');
    }

    selectProduct(e){
        const img = $(e.target).find('option:selected').data('img');

        $(this.imageTarget).fadeOut(250, function (){
            $(this).attr('src', img);
        });
        $(this.imageTarget).fadeIn(250);
    }

    send(e){
        e.preventDefault();
        e.stopImmediatePropagation();

        if(Validator.isValidForm('form[name="order"]')) {
            const target = $(e.target);
            target.after('<div class="full-screen-loader"></div>');

            $.ajax({
                url: target.data('ref'),
                method: 'POST',
                dataType: 'json',
                data: $(e.target).parents('form').serialize(),
                success: function (response){
                    target.siblings('.full-screen-loader').remove();
                    InternalResponseHandler.showHandledMessage(response);

                    if(response.code === InternalResponseHandler.success){
                        $(e.target).parents('form').trigger('reset');
                    }
                },
                error: function (){
                    target.siblings('.full-screen-loader').remove();
                    errorToast('Usługa jest chwilowo niedostępna.');
                }
            });
        } else {
            errorToast('Wszystkie pola formularza muszą być wypełnione.');
        }
    }
}