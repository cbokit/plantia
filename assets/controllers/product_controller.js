import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
    static targets = ['productBlock', 'products'];

    connect() {
        window.addEventListener('scroll', function() {
            let block = document.querySelector('.phone_info_fixed');
            let rectStart = document.querySelector('.product_hero').getBoundingClientRect();
            let rectEnd = document.querySelector('#kontakt').getBoundingClientRect();

            if (rectStart.bottom + 40 <= window.innerHeight) {
                block.style.display = 'block';
            } else {
                block.style.display = 'none';
            }

            if(rectEnd.top + 210 <= window.innerHeight){
                block.style.display = 'none';
            }
        });
    }

    select(e){
        const target = $(e.target).hasClass('product-list-item') === true
            ? $(e.target)
            : $(e.target).parents('.product-list-item');

        if(target.hasClass('active-product')){
            return;
        }

        const showBlockNr = target.data('key');
        let block = null;

        $.each($(this.productBlockTargets), function (k,v){
            if (showBlockNr === $(v).data('key')) {
                block = $(v);
            }
        });

        $(this.productBlockTargets).each(function (k,v) {
            if($(v).hasClass('d-flex')){
                $(v).fadeOut(250, function (){
                    $(this).removeClass('d-flex');
                    $(this).addClass('d-none');
                });

                block.fadeIn(250, function (){
                    $(this).removeClass('d-none');
                    $(this).addClass('d-flex');
                });
            }
        });

        $(this.productsTarget).find('.product-list-item').each(function (){
            $(this).removeClass('active-product');
        });
        target.addClass('active-product');
    }
}