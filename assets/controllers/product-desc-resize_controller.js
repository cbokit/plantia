import { Controller } from '@hotwired/stimulus';
import { useResize } from 'stimulus-use'

export default class extends Controller {
    static targets = ['productDescription']

    connect(){
        useResize(this);

        const windowWidth = window.innerWidth;
        const descriptions = this.productDescriptionTargets;
        let maxH = 0;

        if(descriptions.length > 0 && windowWidth >= 576){
            descriptions.forEach((v) => {
                maxH = maxH < v.offsetHeight ? v.offsetHeight : maxH;
            });

            descriptions.forEach((v) => {
                v.style.height = maxH+'px';
            });
        }
    }

    resize(){
        const descriptions = this.productDescriptionTargets;
        let maxH = 0;

        if(window.innerWidth >= 576) {
            if (descriptions.length > 0) {
                descriptions.forEach((v) => {
                    v.style.height = '100%';
                });

                descriptions.forEach((v) => {
                    maxH = maxH < v.offsetHeight ? v.offsetHeight : maxH;
                });

                descriptions.forEach((v) => {
                    v.style.height = maxH + 'px';
                });
            }
        } else {
            descriptions.forEach((v) => {
                v.style.height = '100%';
            });
        }
    }
}