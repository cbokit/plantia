import { Controller } from '@hotwired/stimulus';
import { useResize } from 'stimulus-use'

export default class extends Controller {
    static targets = ['box', 'imgContainer', 'baseImg'];

    big_photo  = [];
    links_photo= [];
    photos_i   = [];

    connect() {
        useResize(this);

        /** Resize carousel main img */
        this.baseImgTarget.style.height = (this.imgContainerTarget.offsetHeight - 15)+'px';

        window.addEventListener('scroll', function() {
            const block = document.querySelector('.phone_info_fixed');
            const rectStart = document.querySelector('#product__full__desc').getBoundingClientRect();
            let rectEnd = document.querySelector('#kontakt').getBoundingClientRect();

            if(window.innerWidth <= 991){
                if (rectStart.bottom + 20 <= window.innerHeight) {
                    block.style.display = 'block';
                } else {
                    block.style.display = 'none';
                }
            } else {
                block.style.display = 'none';
            }

            if(rectEnd.top + 210 <= window.innerHeight){
                block.style.display = 'none';
            }
        });

        this.boxTargets.forEach(element => {
            this.big_photo.push(element.querySelector('.box_realisation__big_photo_img'));
            this.links_photo.push(element.querySelectorAll('.box_realisation__photos'));
            this.photos_i.push(0);
        });
    }

    resize(){
        this.baseImgTarget.style.height = (this.imgContainerTarget.offsetHeight - 15)+'px';
    }

    prev(e){
        const id_array_realisation = this.getArrayIndexRealisation(e.target);

        if(this.photos_i[id_array_realisation] <= 0){
            this.photos_i[id_array_realisation] = this.links_photo[id_array_realisation].length;
        }

        this.photos_i[id_array_realisation]--;

        return this.setPhotos(id_array_realisation);
    }

    next(e){
        const id_array_realisation = this.getArrayIndexRealisation(e.target);

        if(this.photos_i[id_array_realisation] >= this.links_photo[id_array_realisation].length-1){
            this.photos_i[id_array_realisation] = -1;
        }

        this.photos_i[id_array_realisation]++;

        return this.setPhotos(id_array_realisation);
    }

    changeSlide(e){
        const target = e.target.parentNode;
        const id_array_realisation = this.getArrayIndexRealisation(target);
        let photosidx;

        this.links_photo[id_array_realisation].forEach((element, index) => {
            if(element.dataset.photos === target.getAttribute('data-photos')){
                photosidx = index;
            }
        });

        this.photos_i[id_array_realisation] = photosidx;

        return this.setPhotos(id_array_realisation);
    }

    getArrayIndexRealisation(element){
        /*let index_array_realisation;

        this.boxTargets.forEach((element_check, index) => {
            if(element_check.id === element.closest('.box_realisation').id){
                index_array_realisation = index;
            }
        });

        return index_array_realisation;*/ //this code used in multiple realization case

        return 0;
    }

    setPhotos(id_array_realisation){
        return this.big_photo[id_array_realisation].setAttribute(
            'src',
            this.links_photo[id_array_realisation][this.photos_i[id_array_realisation]].getAttribute('data-photos')
        );
    }
}