import Shuffle from "shufflejs";

let shuffleInstance = new Shuffle(document.getElementById('grid'), {
    itemSelector: '.grid-item',
});

$('select.form-select').on('change', function() {
    shuffleInstance.filter($(this).find('option:selected').data('group'));
});