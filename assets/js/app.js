import Aos from "aos/src/js/aos";
import { jarallax, jarallaxVideo} from "jarallax/dist/jarallax.esm.min";

import '../styles/app.min.css';
import 'aos/dist/aos.css';
import 'izitoast/dist/css/iziToast.min.css';
import 'popup-validation/bin/validation.min.css';

jarallaxVideo();
jarallax(document.querySelectorAll('.jarallax'));
Aos.init();

document.addEventListener('DOMContentLoaded', function () {
    let tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    let tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    });
});

window.addEventListener('resize', function() {
    jarallax(document.querySelectorAll('.jarallax'), 'destroy');
    jarallax(document.querySelectorAll('.jarallax'));
});