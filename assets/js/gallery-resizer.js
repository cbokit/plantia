$(document).ready(function(){
    let galleryItem = $(this).find('.gallery-item');

    if(galleryItem.length > 0){
        galleryItem.each(function (key, value){
            let width = $(value).width();

            $(value).css('height', width);
        });
    }

    $(window).resize(function (){
        if(galleryItem.length > 0){
            galleryItem.each(function (key, value){
                let width = $(value).width();

                $(value).css('height', width);
            });
        }
    });
});