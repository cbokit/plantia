init($('#large-img-file .article-file'), (typeof articleImgLarge !== 'undefined') ? articleImgLarge : null);
init($('#small-img-file .article-file'), (typeof articleImgSmall !== 'undefined') ? articleImgSmall : null);
init($('#small-og-img-file .article-file'), (typeof articleImgOgSmall !== 'undefined') ? articleImgOgSmall : null);
init($('#property-icon .file'), (typeof propertyIcon !== 'undefined') ? propertyIcon : null);
init($('#category-img .file'), (typeof categoryImg !== 'undefined') ? categoryImg : null);
init($('#category-main-img .file'), (typeof categoryBgImg !== 'undefined') ? categoryBgImg : null);
init($('#product-main-img .file'), (typeof productImg !== 'undefined') ? productImg : null);
init($('#product-obj-img .file'), (typeof productObj !== 'undefined') ? productObj : null);
init($('#product-psd-img .file'), (typeof productPsd !== 'undefined') ? productPsd : null);
init($('#realization-main-image .file'), (typeof realizationImg !== 'undefined') ? realizationImg : null);
init($('#gallery-image .file'), (typeof galleryImg !== 'undefined') ? galleryImg : null);

function init(file, name){
    if(file.length === 0 && !name){
        return;
    }

    if(name){
        if(name.constructor.name !== 'Array'){
            file.fileinput({
                showUpload: false,
                allowedFileExtensions: ['png', 'jpg', 'svg', 'obj', 'psd', 'jpeg'],
                initialPreview: "<img src='/uploads/"+name+"' class='file-preview-image' alt='Upload File'>",
            });
        } else {
            let imgs = [];

            name.forEach((element) => {
                imgs.push("<img src='/uploads/" + element + "' class='file-preview-image' alt='Upload File'>");
            });

            file.fileinput({
                showUpload: false,
                allowedFileExtensions: ['png', 'jpg', 'svg', 'obj', 'psd', 'jpeg'],
                initialPreview: imgs,
            });
        }
    } else {
        file.fileinput({
            showUpload: false,
            allowedFileExtensions: ['png', 'jpg', 'svg', 'obj', 'psd', 'jpeg'],
        });
    }
}