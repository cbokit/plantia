$(document).ready(function(){
    let group_realisations = document.querySelectorAll('.box_realisation');
    let big_photo = new Array();
    let links_photo = new Array();
    let photos_i = new Array();

    group_realisations.forEach(element => {
        big_photo.push(element.querySelector('.box_realisation__big_photo_img'));
        links_photo.push(element.querySelectorAll(".box_realisation__photos"));
        photos_i.push(0);
    });

    $('.btn__prev').on('click', function(){
        let id_array_realisation = getArrayIndexRealisation(this);

        if(photos_i[id_array_realisation] <= 0)
            photos_i[id_array_realisation] = links_photo[id_array_realisation].length;
        photos_i[id_array_realisation]--;

        return setPhotos(id_array_realisation);
    });

    $('.btn__next').on('click', function (){
        let id_array_realisation = getArrayIndexRealisation(this);

        if(photos_i[id_array_realisation] >= links_photo[id_array_realisation].length-1)
            photos_i[id_array_realisation] = -1;
        photos_i[id_array_realisation]++;

        return setPhotos(id_array_realisation);
    });

    $('.box_realisation__photos').on('click', function (){
        let id_array_realisation = getArrayIndexRealisation(this);
        let photosidx;

        links_photo[id_array_realisation].forEach((element, index) => {
            if(element.dataset.photos === this.getAttribute('data-photos'))
                photosidx = index;
        });

        photos_i[id_array_realisation] = photosidx;
        return setPhotos(id_array_realisation);
    });

    function getArrayIndexRealisation(element){
        let index_array_realisation;

        group_realisations.forEach((element_check, index) => {
            if(element_check.id === element.closest('.box_realisation').id)
                index_array_realisation = index;
        });

        return index_array_realisation;
    }

    function setPhotos(id_array_realisation){
        return big_photo[id_array_realisation].setAttribute('src', links_photo[id_array_realisation][photos_i[id_array_realisation]].getAttribute('data-photos'));
    }
});